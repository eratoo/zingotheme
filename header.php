<!--    Theme Name: Zingo Cash
        Author: Erato
        Description: Zingo Responsive Theme 
        Version: 1.0
        Text Domain: zingo
 -->
<!DOCTYPE html>
<html>
    <head>	
        <meta charset="utf-8">
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php wp_head(); ?>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">  <!-- in sass -->     
    </head>    
    <body <?php body_class(); ?>>
        <div class="overlay-menu position-fixed overflow-hidden h-100 t0 r0">   
            <div class="close-btn text-center text-white position-absolute"><a>✕</a></div>
            <div class="overlay-content text-center text-uppercase position-absolute w-100 t0 b0">   
                <div class="p-0 mb-5"><a href="<?php echo home_url(); ?>">Home</a></div>             
                <?php wp_nav_menu(
                        array(
                            'container' => false,
                            'menu_class'=> 'm-0 p-0',
                            'theme_location' => 'main-menu'/* ,
                            'menu' => 'main-menu'  */                                        
                        )
                    );
                ?> 
                <div class="mt-5 mb-4"><button class="apply-button button">Apply Today</button></div>
            </div> 
        </div>
        <header>     
           <div class="login-header font-weight-bold">
                <div class="container">
                    <div class="row">
                        <div class="col-12 text-right">
                            <ul class="m-0 p-0">
                                <li><a href="#">
                                    <i class="d-block d-sm-none fas fa-user"></i>
                                    <span class="d-none d-sm-block" id="user-text">Login</span></a></li>      
                                <li><a href="tel:1-888-ZINGO-55">
                                    <i class="d-block d-sm-none fas fa-phone"></i>
                                    <span class="d-none d-sm-block" id="phone-text">CALL US 1-888-ZINGO-55<span></a></li> 
                                <li><a href="facebook"><img alt="social" src="<?php echo get_template_directory_uri(); ?>/pictures/fb.png"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
           </div>            
           <div class="main-header">  <!-- or header child 2 -->            
                <div class="container">                
                    <div class="row align-items-center">                       
                        <div class="logo col-6 col-sm-9 col-md-10 col-lg-2 col-xl-2">
                            <a href="<?php echo home_url(); ?>" >
                                <?php 
                                    if ( function_exists( 'the_custom_logo' ) && has_custom_logo() ) : 
                                        $custom_logo_id = get_theme_mod( 'custom_logo' );
                                        $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                                        echo '<img src="' .$image[0]. '" >';
                                    else : 
                                        echo '<img src=" '.get_template_directory_uri().'/pictures/logo.png " >';
                                    endif;
                                ?>
                            </a>
                        </div>                                         
                        <div class="main-navigation d-none d-lg-block col-0 col-sm-0 col-md-8 col-lg-8 col-xl-8 text-right">                        
                            <div id="main-menu">
                                <?php wp_nav_menu(
                                        array(
                                            'container' => false,
                                            'menu_class'=> 'm-0 p-0',                                               
                                            'theme_location' => 'main-menu'/* ,
                                            'menu' => 'main-menu'  */                                        
                                        )
                                    );
                                ?> 
                            </div>
                        </div>                                               
                        <div class="d-none d-lg-block col-6 col-sm-3 col-md-2 col-lg-2 col-xl-2 text-right">                      
                            <button class="apply-button button">Apply Today</button>
                        </div>                 
                        <div class="collapse d-block d-lg-none col-6 col-sm-3 col-md-2 col-lg-2 col-xl-2 text-right">                      
                            <button class="collapse-button button"><i class="fas fa-bars"></i></button> <!-- .button / expand btn -->            
                        </div>                         
                    </div>
                </div>
           </div>  
        </header>        
        <main>   
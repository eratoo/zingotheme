<section class="icons-map">
    <div class="container">              
        <div class="row text-center heading"> 
            <div class="col-12">   
                <div class="icons-heading-wrapper">         
                    <h3>OUR CASH PROCESS</h3>
                </div>
            </div>
        </div>
        <div class="row text-center justify-content-center">           
            <div class="col-12 col-sm-3 col-md-3 icon-stack arrow pb-2 pb-sm-0">                  
                <span class="fa-stack map-icon"> 
                    <i class="fa fa-circle fa-stack-2x icon-background"></i>
                    <i class="far fa-file-alt fa-stack-1x icon"></i>
                </span>                        
                <p class="d-inline-block d-sm-block icon-text text-left text-sm-center">Enroll Today</p> 
            </div>
            <div class="col-12 col-sm-3 col-md-3 icon-stack arrow pb-2 pb-sm-0">                  
                <span class="fa-stack map-icon">
                    <i class="fa fa-circle fa-stack-2x icon-background"></i>
                    <i class="fas fa-desktop fa-stack-1x icon"></i>
                </span>
                <p class="d-inline-block d-sm-block icon-text text-left text-sm-center">Apply Online</p>           
            </div>    
            <div class="col-12 col-sm-3 col-md-3 icon-stack arrow pb-2 pb-sm-0">                  
                <span class="fa-stack map-icon">
                    <i class="fa fa-circle fa-stack-2x icon-background"></i>
                    <i class="far fa-thumbs-up fa-stack-1x icon"></i>
                </span>
                <p class="d-inline-block d-sm-block icon-text text-left text-sm-center">Verification</p>              
            </div>
            <div class="col-12 col-sm-3 col-md-3 icon-stack">                  
                <span class="fa-stack map-icon">
                    <i class="fa fa-circle fa-stack-2x icon-background"></i>
                    <i class="fas fa-dollar-sign fa-stack-1x icon"></i>
                </span>
                <p class="d-inline-block d-sm-block icon-text text-left text-sm-center">Zingo Cash!</p>          
            </div>            
        </div>
    </div>
</section>

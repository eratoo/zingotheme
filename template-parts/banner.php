<?php 
    $banner_image = get_field('banner-background');
    $banner_heading = get_field('banner-title');
    $banner_text = get_field('banner-description');    
?>
<section class="banner-div position-relative"> <!-- change var name / sass for template-parts specifically / section in homepage.php-->
    <div class="banner hero-bg position-relative" style="background-image: url('<?php if($banner_image) { echo $banner_image;} else { echo get_template_directory_uri(). "/pictures/banner.png"; } ?>');"></div><!-- peritto banner banner div -->
    <div  class="banner-text text-center text-lg-left w-100 b0">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">                        
                    <h2 class="heading font-weight-normal"><?php echo $banner_heading ?></h2>
                    <p class="secondary"><?php echo $banner_text ?></p>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-0 col-sm-0 col-0">
                    <button class="enroll-button button">Enroll Today</button>
                </div>
            </div>
        </div>
    </div>
</section>
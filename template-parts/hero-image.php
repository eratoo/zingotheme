<?php 
    $hero_image = get_field('hero-image');
    $hero_heading = get_field('hero-title');
    $hero_text = get_field('hero-description');
?>
<section class="hero-bg hero" style="background-image: linear-gradient(rgba(0, 0, 0, 0.45),rgba(0, 0, 0, 0.45)), url( <?php if($hero_image) { echo $hero_image;} else { echo get_template_directory_uri(). "/pictures/hero.png"; } ?> );">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-6 text-container text-center text-lg-left">
                <h2 class="font-weight-bold text-uppercase"><?php echo $hero_heading ?></h2>
                <p class="secondary"><?php echo $hero_text ?></p>
            </div>
            <!-- <div class="col-0 col-sm-0 col-md-0 col-lg-3 col-xl-6"> </div> -->
        </div>
    </div>
</section>
        </main>
        <footer>
            <div class="footer-top text-center text-sm-left pt-4 pb-4 pt-md-5 pb-md-5"> <!-- pt, pb in sass -->
                <div class="container">
                    <div class="row d-block d-md-none logo-replacement text-center text-uppercase text-semi-bold mt-2"> 
                        <div class="col-sm-12 col-12"><?php bloginfo('name') ?><hr></div>
                    </div>
                    <div class="row mt-2 mb-2">
                        <div class="footer-logo d-none d-md-block col-xl-3 col-lg-3 col-md-3 col-sm-0 col-0 text-xl-left font-weight-bold">
                            <a href="<?php echo home_url(); ?>" >
                                <?php 
                                    if ( function_exists( 'the_custom_logo' ) && has_custom_logo() ) : 
                                        $custom_logo_id = get_theme_mod( 'custom_logo' );
                                        $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                                        echo '<img src="' .$image[0]. '" >';
                                    else : 
                                        echo '<img src=" '.get_template_directory_uri().'/pictures/logo.png " >';
                                    endif;
                                ?>
                            </a>
                            <p class="mb-0 footer-logo-text">1-888-ZINGO-55</p>                          
                        </div>                            
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-12">
                            <ul>   
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Instructions</a></li>
                                <li><a href="#">Rates & Terms</a></li>
                                <li><a href="#">FAQ</a></li><br>
                                <li><a href="#">Contact Us</a></li>
                            </ul>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-12">
                            <ul>   
                                <li><a href="#"><strong>Learn More</strong></a></li>
                                <li><a href="#">Budget Tips</a></li>
                                <li><a href="#">Consumer Loan Pamphlet</a></li>
                                <li><a href="#">Credit Resources</a></li> 
                                <li><a href="#">Glossary of Terms</a></li>                           
                            </ul>
                        </div>	
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-12">                                
                            <ul>   
                                <li><a href="#"><strong>My Account</strong></a></li>
                                <li><a href="#">Application History</a></li>
                                <li><a href="#">Payment History</a></li>
                                <li><a href="#">Personal Information</a></li> 
                                <li><a href="#">Change Password</a></li>
                                <li><a href="#">Apply</a></li> 
                                <li><a href="#">Logout</a></li>                            
                            </ul>
                        </div>                       
                    </div>                  
                </div>
            </div>
            <div class="footer-last font-weight-bold text-uppercase pt-3 pb-3">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-7 footer-last-navigation text-center text-lg-left">
                            <?php wp_nav_menu(
                                array(
                                        'container' => false,
                                        'menu_class'=> 'm-0 p-0',                                         
                                        'theme_location' => 'footer-last'
                                        /* 'menu' => 'footer-last'  */                                        
                                    )
                                );
                            ?> 
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5 footer-last-copyright text-center text-lg-right"><a>©2011 Zingo Financial, LLC. All rights reserved.</a></div>
                    </div>
                </div>
            </div>
        </footer>
        <?php wp_footer(); ?>
    </body> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
</html>

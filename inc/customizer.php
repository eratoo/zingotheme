<?php
function zingo_customize_register( $wp_customize ) {

    /*******************************************
    
    Color Settings
    https://code.tutsplus.com/tutorials/settings-and-controls-for-a-color-scheme-in-the-theme-customizer--cms-21350
    https://code.tutsplus.com/tutorials/adding-the-css-for-a-color-scheme-in-the-theme-customizer--cms-21351
    
    ********************************************/
    $wp_customize->add_section( 'colors' , array(
        'title' =>  'Color Settings',
    ) );

      $colors[] = array(
        'slug'=>'main_color', 
        'default' => '#6b6b6b',
        'label' => 'Main Color'
    );

    $colors[] = array(
        'slug'=>'icon_bg', 
        'default' => '#85ad3e',
        'label' => 'Icon Background'
    );
    
    $colors[] = array(
        'slug'=>'top_navigation_bg', 
        'default' => '#d7e4e7',
        'label' => 'Top Navigation Bar'
    );
   
    $colors[] = array(
        'slug'=>'text_color', 
        'default' => '#8e8e8e',
        'label' => 'Text Color'
    );

    foreach( $colors as $color ) {
    
        // SETTINGS
        $wp_customize->add_setting(
            $color['slug'], array(
                'default' => $color['default'],
                'type' => 'option', 
                'capability' =>  'edit_theme_options'
            )
        );
        // CONTROLS
        $wp_customize->add_control(
            new WP_Customize_Color_Control(
                $wp_customize,
                $color['slug'], 
                array('label' => $color['label'], 
                'section' => 'colors',
                'settings' => $color['slug'])
            )
        );
    } 
}
add_action( 'customize_register', 'zingo_customize_register' );

function zingo_customize_colors() {

    $main_color = get_option( 'main_color' );
    
    $icon_bg = get_option( 'icon_bg' );
    
    $top_navigation_bg = get_option( 'top_navigation_bg' );
    
    $text_color = get_option( 'text_color' );


    ?>
    <style>   
        header a { 
            color:  <?php echo $main_color; ?>; 
        }
        .icon-background {
            color:  <?php echo $icon_bg; ?>; 
        }
        .login-header {
            background-color: <?php echo $top_navigation_bg; ?>;
        }
        #icon-text {
            color: <?php echo $text_color; ?>;  
        }   
    </style>    
<?php

}
add_action( 'wp_head', 'zingo_customize_colors' );
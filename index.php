<?php get_header(); ?>
    <div class="container-fluid"> 
        <div class="row d-flex justify-content-center pt-5 pb-5">
            <div class="col-12 heading text-capitalize text-center align-items-center">           
                <h1 class=" font-weight-bold"><?php the_title(); ?></h1>
            </div>
        </div>
        <div class="row d-flex justify-content-center pb-5">
            <div class="col-10 col-sm-9 col-md-9 col-lg-9 col-xl-7 content">
                <div class="text-left" style><p><?php if ( have_posts() ) : while ( have_posts() ) : the_post(); the_content(); endwhile; endif; ?></p></div>
            </div>
        </div>
    </div> 
<?php get_footer(); ?>

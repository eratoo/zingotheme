<?php
/* wp menus */
function my_menus() {
  $locations = array(
    'main-menu' => __( 'Primary', 'zingotheme' ),
    'footer-last' => __( 'Footer Last', 'zingotheme' )
  );
  register_nav_menus( $locations );
}
add_action( 'init', 'my_menus' );

/* css */
add_action( 'wp_enqueue_scripts', 'styles' );
function styles() {
  /* css */
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css' ); 
  /* jquery */
    wp_deregister_script('jquery');
  /* mobile collapsible menu */
    wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js', array(), null, true);
    wp_enqueue_script( 'mobile-menu', get_template_directory_uri() . '/js/collapse-menu' . $suffix . '.js', array('jquery'), '3.4.1', true );
}
/* hide admin bar from admin */
function hide_admin_bar_from_front_end(){
    if (is_blog_admin()) {
      return true;
    }
    return false;
  }
add_filter( 'show_admin_bar', 'hide_admin_bar_from_front_end' );

/* Wordpress Color Settings / disabled */
/* include_once( 'inc/customizer.php' );*/

function theme_slug_setup() {
  add_theme_support( 'title-tag' );
  add_theme_support( 'custom-logo' );
}
add_action( 'after_setup_theme', 'theme_slug_setup' );